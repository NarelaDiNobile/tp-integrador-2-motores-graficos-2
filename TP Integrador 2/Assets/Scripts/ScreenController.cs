using UnityEngine;
using TMPro;

public class ScreenController : MonoBehaviour
{
    public float displayTime = 5f;
    public TMP_Text dialogueText;
    public float letterDelay = 0.1f;

    private bool dialogueTriggered = false;

    private void OnTriggerEnter(Collider other)
    {
        if (!dialogueTriggered && other.CompareTag("Player"))
        {
            dialogueTriggered = true;
            dialogueText.gameObject.SetActive(true);
            dialogueText.text = "Player 010";

            StartCoroutine(DisplayDialogueText(dialogueText.text));
        }
    }

    private System.Collections.IEnumerator DisplayDialogueText(string fullText)
    {
        for (int i = 0; i <= fullText.Length; i++)
        {
            dialogueText.text = fullText.Substring(0, i);
            yield return new WaitForSeconds(letterDelay);
        }

        yield return new WaitForSeconds(displayTime);

        dialogueText.gameObject.SetActive(false);
    }
}
