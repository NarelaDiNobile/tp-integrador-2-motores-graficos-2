using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MenuPausa2 : MonoBehaviour
{
    public GameObject pauseMenu1;
    public string NombreDeEscena1;
    public bool Mouse1;

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            Mouse1 = !Mouse1;
            if (Mouse1 == false)
            {
                pauseMenu1.SetActive(false);
                AudioListener.pause = false;
                Time.timeScale = 1;
                Cursor.visible = true;
                Cursor.lockState = CursorLockMode.None;
            }
            if (Mouse1 == true)
            {
                pauseMenu1.SetActive(true);
                AudioListener.pause = true;
                Time.timeScale = 0;
                Cursor.visible = true;
                Cursor.lockState = CursorLockMode.None;
            }
        }
    }
    public void resumeGame1()
    {
        Mouse1 = false;
        pauseMenu1.SetActive(false);
        AudioListener.pause = false;
        Time.timeScale = 1;
        Cursor.visible = true;
        Cursor.lockState = CursorLockMode.None; // Aqu�, cambiamos de Locked a None
    }

    public void quitToMenu1()
    {
        Time.timeScale = 1;
        AudioListener.pause = false;
        SceneManager.LoadScene(NombreDeEscena1);
    }
    public void quitToDesktop1()
    {
        Time.timeScale = 1;
        AudioListener.pause = false;
        Debug.Log("Estas en el Desktop");
        Application.Quit();
    }
}
