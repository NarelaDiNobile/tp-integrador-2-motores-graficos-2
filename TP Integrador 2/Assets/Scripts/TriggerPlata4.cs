using System.Collections;
using TMPro;
using UnityEngine;

public class TriggerPlata4 : MonoBehaviour
{
    public TMP_Text counterText4;
    public float incrementSpeed4 = 8000000f;
    public int targetAmount4 = 100000000;

    public AudioSource audioSource4;
    public AudioClip incrementSound4;

    private bool isTriggered4 = false;
    private int currentAmount4 = 0;

    private void Start()
    {
        audioSource4 = GetComponent<AudioSource>();
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player") && !isTriggered4)
        {
            isTriggered4 = true;
            StartCoroutine(IncrementCounter());
        }
    }

    private IEnumerator IncrementCounter()
    {
        audioSource4.clip = incrementSound4;
        audioSource4.loop = true;
        audioSource4.Play();

        while (currentAmount4 < targetAmount4)
        {
            currentAmount4 += Mathf.RoundToInt(incrementSpeed4 * Time.deltaTime);
            currentAmount4 = Mathf.Clamp(currentAmount4, 0, targetAmount4);
            counterText4.text = "$" + currentAmount4.ToString("N0");
            yield return null;
        }

        audioSource4.Stop();

        yield return new WaitForSeconds(5f); // Pausa de 2 segundos antes de ocultar el texto

        counterText4.gameObject.SetActive(false);
    }
}
