using System.Collections;
using TMPro;
using UnityEngine;

public class TriggerPlata2 : MonoBehaviour
{
    public TMP_Text counterText2;
    public float incrementSpeed2 = 90000f;
    public int targetAmount2 = 1000000;

    public AudioSource audioSource2;
    public AudioClip incrementSound2;

    private bool isTriggered2 = false;
    private int currentAmount2 = 0;

    private void Start()
    {
        audioSource2 = GetComponent<AudioSource>();
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player") && !isTriggered2)
        {
            isTriggered2 = true;
            StartCoroutine(IncrementCounter());
        }
    }

    private IEnumerator IncrementCounter()
    {
        audioSource2.clip = incrementSound2;
        audioSource2.loop = true;
        audioSource2.Play();

        while (currentAmount2 < targetAmount2)
        {
            currentAmount2 += Mathf.RoundToInt(incrementSpeed2 * Time.deltaTime);
            currentAmount2 = Mathf.Clamp(currentAmount2, 0, targetAmount2);
            counterText2.text = "$" + currentAmount2.ToString("N0");
            yield return null;
        }

        audioSource2.Stop();

        yield return new WaitForSeconds(5f); // Pausa de 2 segundos antes de ocultar el texto

        counterText2.gameObject.SetActive(false);
    }
}
