using UnityEngine;
using TMPro;
using UnityEngine.SceneManagement;

public class TriggerInteractivo : MonoBehaviour
{
    public TMP_Text interactionText;
    public GameObject panel;
    public string menuSceneName;

    private bool playerInRange;

    private void Start()
    {
        interactionText.gameObject.SetActive(false);
        panel.SetActive(false);
    }

    private void Update()
    {
        if (playerInRange && Input.GetKeyDown(KeyCode.E))
        {
            ReturnToMenu();
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            interactionText.gameObject.SetActive(true);
            panel.SetActive(true);
            playerInRange = true;
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            interactionText.gameObject.SetActive(false);
            panel.SetActive(false);
            playerInRange = false;
        }
    }

    private void ReturnToMenu()
    {
        SceneManager.LoadScene("Menu");
    }
}

