using UnityEngine;

public class TriggerBilleteFinal : MonoBehaviour
{
    public GameObject billetesObject;
    public GameObject Trofeo;
    public GameObject Particulas;
    public AudioSource audioSource;
    public AudioClip activationSound;

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            ActivateBilletesAnimations();
            PlayActivationSound();
        }
    }

    private void ActivateBilletesAnimations()
    {
        billetesObject.SetActive(true);
        Trofeo.SetActive(true);
        Particulas.SetActive(true);
    }

    private void PlayActivationSound()
    {
        audioSource.clip = activationSound;
        audioSource.Play();
    }
}

