using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using UnityEngine.Audio;
using TMPro;

public class Juego2Unlock : MonoBehaviour
{
    public List<Button> buttons;
    public List<Button> shuffledButtons;
    int counter = 0;
    public TMP_Text timerText;
    public Text gameOverText;
    public Text roundCompleteText;
    private float timeRemaining = 20f;
    private Coroutine timerCoroutine;
    private int roundsCompleted = 0;

    public AudioSource clickSound;
    public AudioSource winSound;
    public AudioSource loseSound; // Agrega el AudioSource para el sonido de perder

    public void Start()
    {
        Cursor.lockState = CursorLockMode.None;
        Cursor.visible = true;
        RestartTheGame();
        StartTimer();
    }

    public void RestartTheGame()
    {
        counter = 0;
        shuffledButtons = buttons.OrderBy(a => Random.Range(0, 100)).ToList();
        for (int i = 1; i < 11; i++)
        {
            shuffledButtons[i - 1].GetComponentInChildren<Text>().text = i.ToString();
            shuffledButtons[i - 1].interactable = true;
            shuffledButtons[i - 1].image.color = new Color32(177, 228, 233, 255);
        }
    }

    public void pressButton(Button button)
    {
        if (int.Parse(button.GetComponentInChildren<Text>().text) - 1 == counter)
        {
            counter++;
            button.interactable = false;
            button.image.color = Color.green;
            if (counter == 10)
            {
                StopTimer();
                StartCoroutine(presentResult(true));
            }
        }
        else
        {
            StopTimer();
            StartCoroutine(presentResult(false));
        }

        clickSound.Play();
    }

    public IEnumerator presentResult(bool win)
    {
        if (!win)
        {
            foreach (var button in shuffledButtons)
            {
                button.image.color = Color.red;
                button.interactable = false;
            }

            // Reproduce el sonido de perder
            loseSound.Play();
        }
        else
        {
            roundsCompleted++;
            if (roundsCompleted >= 3)
            {
                roundCompleteText.gameObject.SetActive(true);
                gameOverText.gameObject.SetActive(false);
                winSound.Play();
                yield return new WaitForSeconds(3f);
                SceneManager.LoadScene("SampleScene");
            }
        }

        yield return new WaitForSeconds(2f);

        if (win)
        {
            RestartTheGame();
            StartTimer();
        }
        else
        {
            SceneManager.LoadScene(SceneManager.GetActiveScene().name);
        }
    }

    private void StartTimer()
    {
        timerCoroutine = StartCoroutine(UpdateTimer());
    }

    private void StopTimer()
    {
        if (timerCoroutine != null)
        {
            StopCoroutine(timerCoroutine);
        }
    }

    private IEnumerator UpdateTimer()
    {
        while (timeRemaining > 0)
        {
            timerText.text = "" + Mathf.RoundToInt(timeRemaining).ToString();

            yield return new WaitForSeconds(1f);

            timeRemaining -= 1f;
        }

        timerText.text = "";
        gameOverText.gameObject.SetActive(true);

        StopTimer();
        StartCoroutine(presentResult(false));
    }
}

