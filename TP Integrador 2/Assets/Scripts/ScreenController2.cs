using UnityEngine;
using UnityEngine.UI;

using TMPro;

public class ScreenController2 : MonoBehaviour
{
    public float displayTime2 = 5f;
    public TMP_Text dialogueText2;
    public float letterDelay2 = 0.1f;

    private bool dialogueTriggered2 = false;

    private void OnTriggerEnter(Collider other)
    {
        if (!dialogueTriggered2 && other.CompareTag("Player"))
        {
            dialogueTriggered2 = true;
            dialogueText2.gameObject.SetActive(true);
            dialogueText2.text = "$ 10.000.000.000";


            StartCoroutine(DisplayDialogueText(dialogueText2.text));
        }
    }

    private System.Collections.IEnumerator DisplayDialogueText(string fullText)
    {
        for (int i = 0; i <= fullText.Length; i++)
        {
            dialogueText2.text = fullText.Substring(0, i);
            yield return new WaitForSeconds(letterDelay2);
        }

        yield return new WaitForSeconds(displayTime2);

        dialogueText2.gameObject.SetActive(false);
        
    }
}
