using System.Collections;
using TMPro;
using UnityEngine;

public class TriggerPlata3 : MonoBehaviour
{
    public TMP_Text counterText3;
    public float incrementSpeed3 = 2000000f;
    public int targetAmount3 = 80000000;

    public AudioSource audioSource3;
    public AudioClip incrementSound3;

    private bool isTriggered3 = false;
    private int currentAmount3 = 0;

    private void Start()
    {
        audioSource3 = GetComponent<AudioSource>();
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player") && !isTriggered3)
        {
            isTriggered3 = true;
            StartCoroutine(IncrementCounter());
        }
    }

    private IEnumerator IncrementCounter()
    {
        audioSource3.clip = incrementSound3;
        audioSource3.loop = true;
        audioSource3.Play();

        while (currentAmount3 < targetAmount3)
        {
            currentAmount3 += Mathf.RoundToInt(incrementSpeed3 * Time.deltaTime);
            currentAmount3 = Mathf.Clamp(currentAmount3, 0, targetAmount3);
            counterText3.text = "$" + currentAmount3.ToString("N0");
            yield return null;
        }

        audioSource3.Stop();

        yield return new WaitForSeconds(5f); // Pausa de 2 segundos antes de ocultar el texto

        counterText3.gameObject.SetActive(false);
    }
}
