using UnityEngine;
using UnityEngine.UI;
using TMPro;
using UnityEngine.SceneManagement;
using System.Collections;

public class TimerController3 : MonoBehaviour
{
    public Canvas canvas1; // Primer Canvas
    public Canvas canvas2; // Segundo Canvas
    public Button sceneChangeButton; // Bot�n para cambiar de escena
    public AudioSource audioSource;
    public AudioClip triggerSound;
    public string sceneToLoad; // Nombre de la escena a cargar al presionar el bot�n

    private bool hasTriggered3 = false;

    private void Start()
    {
        canvas1.enabled = false;
        canvas2.enabled = false;

        // Configura el bot�n para cambiar a la nueva escena cuando se presione
        sceneChangeButton.onClick.AddListener(ChangeScene);
    }

    private void Update()
    {
        if (canvas2.enabled)
        {
            if (Input.GetKeyDown(KeyCode.Escape))
            {
                SceneManager.LoadScene("MenuPrincipal");
            }
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player") && !hasTriggered3)
        {
            ActivateBlackScreen();
        }
    }

    private void ActivateBlackScreen()
    {
        canvas1.enabled = true;
        Camera.main.backgroundColor = Color.black;

        StartCoroutine(ActivateSecondCanvasAfterDelay(5f)); // 5f es el retraso en segundos para activar el segundo canvas
    }

    private IEnumerator ActivateSecondCanvasAfterDelay(float delay)
    {
        audioSource.PlayOneShot(triggerSound); // Reproduce el sonido una vez

        yield return new WaitForSeconds(triggerSound.length); // Espera hasta que termine de reproducirse el sonido

        yield return new WaitForSeconds(delay); // Espera el retraso especificado

        canvas2.enabled = true; // Activa el segundo Canvas

        // Activa el cursor
        Cursor.lockState = CursorLockMode.None;
        Cursor.visible = true;
    }

    // M�todo para cambiar a la nueva escena
    private void ChangeScene()
    {
        SceneManager.LoadScene(sceneToLoad);
    }
}
