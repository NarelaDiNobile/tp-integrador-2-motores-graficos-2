using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class Brillo : MonoBehaviour
{
    public Slider slider2;
    public float sliderValue2;
    public Image panelBrillo2;

    // Start is called before the first frame update
    void Start()
    {
        slider2.value = PlayerPrefs.GetFloat("brillo", 0.5f);

        panelBrillo2.color = new Color(panelBrillo2.color.r, panelBrillo2.color.g, panelBrillo2.color.b, slider2.value);
    }

    //Update is called once per frame
    void Update()
    {

    }

    public void ChangeSlider(float valor)
    {
        sliderValue2 = valor;
        PlayerPrefs.SetFloat("brillo", sliderValue2);
        panelBrillo2.color = new Color(panelBrillo2.color.r, panelBrillo2.color.g, panelBrillo2.color.b, slider2.value);
    }
}