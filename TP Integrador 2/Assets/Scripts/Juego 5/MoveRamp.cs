using UnityEngine;

public class MoveRamp : MonoBehaviour
{
    public float tiltSpeed = 1.0f;
    public float maxTiltAngle = 30.0f;

    private float tiltAmountX = 0f;
    private float tiltAmountY = 0f;

    void Update()
    {
        float mouseX = Input.GetAxis("Mouse X");
        float mouseY = Input.GetAxis("Mouse Y");

        float tiltChangeX = mouseX * tiltSpeed;
        float tiltChangeY = mouseY * tiltSpeed;

        // Limit the tilt changes
        tiltChangeX = Mathf.Clamp(tiltChangeX, -maxTiltAngle, maxTiltAngle);
        tiltChangeY = Mathf.Clamp(tiltChangeY, -maxTiltAngle, maxTiltAngle);

        // Apply the tilt changes
        tiltAmountX += tiltChangeX;
        tiltAmountY += tiltChangeY;

        // Limit the total tilt amount
        tiltAmountX = Mathf.Clamp(tiltAmountX, -maxTiltAngle, maxTiltAngle);
        tiltAmountY = Mathf.Clamp(tiltAmountY, -maxTiltAngle, maxTiltAngle);

        // Apply the tilt
        transform.localRotation = Quaternion.Euler(-tiltAmountY, 0f, -tiltAmountX);
    }
}
