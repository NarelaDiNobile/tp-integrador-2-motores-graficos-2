using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System.Collections;

public class GameManager : MonoBehaviour
{
    public Image[] heartImages;
    public int maxLives = 3;
    private int currentLives;

    public GameObject panelPerdiste;
    public GameObject panelReiniciar;
    public GameObject panelGanaste;
    public CountdownTimer countdownTimer; // Referencia al script CountdownTimer

    private int greenCylindersCount;
    private bool isGameInProgress = true; // Variable para controlar si el juego est� en progreso

    private void Start()
    {
        currentLives = maxLives;
        UpdateHeartDisplay();
        panelPerdiste.SetActive(false);
        panelReiniciar.SetActive(false);
        panelGanaste.SetActive(false);
    }

    public void DecreaseLives(int amount)
    {
        currentLives -= amount;
        currentLives = Mathf.Clamp(currentLives, 0, maxLives);
        UpdateHeartDisplay();

        if (currentLives <= 0)
        {
            isGameInProgress = false; // Detener el juego al perder todas las vidas
            panelPerdiste.SetActive(true);
            panelReiniciar.SetActive(true);
        }
    }

    public void IncrementGreenCylindersCount()
    {
        greenCylindersCount++;

        if (greenCylindersCount >= 10)
        {
            isGameInProgress = false; // Detener el juego al ganar
            panelGanaste.SetActive(true);
            panelReiniciar.SetActive(false);
            StartCoroutine(ChangeSceneAfterDelay(3f)); // Cambiar la escena despu�s de 3 segundos
        }
    }

    public void ShowPerdistePanel()
    {
        panelPerdiste.SetActive(true);
        panelReiniciar.SetActive(true);
    }

    private void UpdateHeartDisplay()
    {
        for (int i = 0; i < heartImages.Length; i++)
        {
            if (i < currentLives)
                heartImages[i].enabled = true;
            else
                heartImages[i].enabled = false;
        }
    }

    IEnumerator ChangeSceneAfterDelay(float delay)
    {
        yield return new WaitForSeconds(delay);
        SceneManager.LoadScene("SampleScene"); // Reemplaza "NombreDeTuEscenaSiguiente" con el nombre de la escena que deseas cargar despu�s de ganar.
    }

    // M�todo para verificar si el juego est� en progreso
    public bool IsGameInProgress()
    {
        return isGameInProgress;
    }
}
