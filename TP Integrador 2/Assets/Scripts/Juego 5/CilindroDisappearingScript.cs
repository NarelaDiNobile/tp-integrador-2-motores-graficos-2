using UnityEngine;

public class CilindroDisappearingScript : MonoBehaviour
{
    // M�todo para manejar la colisi�n
    private void OnCollisionEnter(Collision collision)
    {
        // Comprobar si la colisi�n proviene de la pelota
        if (collision.gameObject.CompareTag("Player"))
        {
            // Si la colisi�n es con la pelota, desactivar el cilindro
            gameObject.SetActive(false);
        }
    }
}
