using UnityEngine;
using TMPro;

public class CountdownTimer : MonoBehaviour
{
    public float timeRemaining = 60f;
    public TMP_Text countdownText;
    private bool isTimerRunning = true;

    private GameManager gameManager;

    private void Start()
    {
        gameManager = FindObjectOfType<GameManager>();
    }

    private void Update()
    {
        // Verificar si el juego est� en progreso antes de actualizar el contador
        if (gameManager.IsGameInProgress())
        {
            if (isTimerRunning && timeRemaining > 0)
            {
                timeRemaining -= Time.deltaTime;
                UpdateCountdownDisplay();
            }
            else
            {
                isTimerRunning = false;
                timeRemaining = 0f;
                countdownText.text = "00:00";
                ShowPerdistePanel();
            }
        }
    }

    private void UpdateCountdownDisplay()
    {
        int minutes = Mathf.FloorToInt(timeRemaining / 60);
        int seconds = Mathf.FloorToInt(timeRemaining % 60);
        countdownText.text = string.Format("{1:00}", minutes, seconds);
    }

    private void ShowPerdistePanel()
    {
        gameManager.ShowPerdistePanel();
    }
}
