using UnityEngine;

public class CylinderBehavior : MonoBehaviour
{
    public bool isExplosive = false;
    public GameObject explosionEffect;
    private GameManager gameManager;

    private void Start()
    {
        gameManager = GameObject.FindObjectOfType<GameManager>();
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            if (isExplosive)
            {
                // Mostrar efecto de explosión (opcional)
                if (explosionEffect != null)
                {
                    Instantiate(explosionEffect, transform.position, Quaternion.identity);
                }

                // Destruir el cilindro y disminuir una vida (si tienes un controlador de vidas)
                Destroy(gameObject);
                gameManager.DecreaseLives(1);
            }
            else
            {
                // Cambiar el color del cilindro a verde
                GetComponent<Renderer>().material.color = Color.green;

                // Incrementar el contador de cilindros verdes en el GameManager
                gameManager.IncrementGreenCylindersCount();
            }
        }
    }
}

