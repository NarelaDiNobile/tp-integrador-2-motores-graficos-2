using UnityEngine;
using UnityEngine.UI;
using TMPro;
using System.Collections;

public class TriggerPlata1 : MonoBehaviour
{
    public TMP_Text counterText;
    public float incrementSpeed = 500000f;
    public int targetAmount = 10000000;

    public AudioSource audioSource;
    public AudioClip incrementSound;

    private bool isTriggered = false;
    private int currentAmount = 0;

    private void Start()
    {
        audioSource = GetComponent<AudioSource>();
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player") && !isTriggered)
        {
            isTriggered = true;
            StartCoroutine(IncrementCounter());
        }
    }

    private IEnumerator IncrementCounter()
    {
        audioSource.clip = incrementSound;
        audioSource.loop = true;
        audioSource.Play();

        while (currentAmount < targetAmount)
        {
            currentAmount += Mathf.RoundToInt(incrementSpeed * Time.deltaTime);
            currentAmount = Mathf.Clamp(currentAmount, 0, targetAmount);
            counterText.text = "$" + currentAmount.ToString("N0");
            yield return null;
        }

        audioSource.Stop();

        yield return new WaitForSeconds(5f); // Pausa de 2 segundos antes de ocultar el texto

        counterText.gameObject.SetActive(false);
    }
}
