using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using UnityEngine.SceneManagement;

public class RobotLogic : MonoBehaviour
{
    public ClilkBoton[] myButtons;
    public List<int> colorList;

    public float showtime = 0.5f;
    public float pausetime = 0.5f;

    private bool robot = false;
    public bool player = false;

    private int myRandom;

    public int level = 2;
    private int playerLevel = 0;

    public Button StartButton;
    public TMP_Text gameOver;
    public TMP_Text Score;
    private int score;

    public GameObject panel;
    public float panelDelay = 5f;
    private bool panelShown = false;

    public GameObject gameOverPanel;

    public Button GameOverButton;


    void Start()
    {
        Cursor.visible = true;
        Cursor.lockState = CursorLockMode.None;

        for (int i = 0; i < myButtons.Length; i++)
        {
            myButtons[i].onClick += ButtonClicked;
            myButtons[i].myNumber = i;
            myButtons[i].myLogic = this;
        }
    }

    void ButtonClicked(int _number)
    {
        if (player)
        {
            if (_number == colorList[playerLevel])
            {
                playerLevel += 1;
                score += 1;
                Score.text = score.ToString();

                if (score >= 30 && !panelShown)
                {
                    StartCoroutine(ShowPanelAndLoadScene());
                }
            }
            else
            {
                GameOver();
            }

            if (playerLevel == level)
            {
                level += 1;
                playerLevel = 0;
                player = false;
                robot = true;
            }
        }
    }

    void Update()
    {
        if (robot)
        {
            robot = false;
            StartCoroutine(Robot());
        }

        if (gameOverPanel.activeSelf && Input.GetKeyDown(KeyCode.R))
        {
            RestartScene();
        }
    }

    private void RestartScene()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }

    private IEnumerator Robot()
    {
        yield return new WaitForSeconds(1f);

        for (int i = 0; i < level; i++)
        {
            if (colorList.Count < level)
            {
                myRandom = Random.Range(0, myButtons.Length);
                colorList.Add(myRandom);
            }

            myButtons[colorList[i]].ClickedColor();
            yield return new WaitForSeconds(showtime);
            myButtons[colorList[i]].UnClickedColor();
            yield return new WaitForSeconds(pausetime);
        }

        player = true;
    }

    public void StartGame()
    {
        robot = true;
        score = 0;
        playerLevel = 0;
        level = 2;
        gameOver.text = "";
        Score.text = score.ToString();
        StartButton.gameObject.SetActive(false);
    }

    private void GameOver()
    {
        gameOver.text = "Game Over";
        StartButton.interactable = true;
        player = false;
        gameOverPanel.SetActive(true);
        GameOverButton.gameObject.SetActive(true);  // Activa el bot�n de GameOver
    }


    private IEnumerator ShowPanelAndLoadScene()
    {
        panelShown = true;
        panel.SetActive(true);
        yield return new WaitForSeconds(panelDelay);
        SceneManager.LoadScene("SampleScene"); // Reemplaza "SampleScene" con el nombre de la escena de destino
    }
}