using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ClilkBoton : MonoBehaviour
{
    public Material lMat;
    public Material nMat;

    private Renderer myR;
    private Vector3 myTP;

    public int myNumber = 99;
    public RobotLogic myLogic;

    public delegate void ClickEV(int number);

    public event ClickEV onClick;

    private void Awake()
    {
        myR = GetComponent<Renderer>();
        myR.enabled = true;
        myTP = transform.position;
    }

    private void Update()
    {

    }

    private void OnMouseDown()
    {
        onClick.Invoke(myNumber);
        transform.position = new Vector3(myTP.x, -.1f, myTP.z);
    }

    private void OnMouseUp()
    {
        UnClickedColor();
        transform.position = new Vector3(myTP.x, myTP.y, myTP.z);
    }

    public void ClickedColor()
    {
        myR.sharedMaterial = nMat;
    }

    public void UnClickedColor()
    {
        myR.sharedMaterial = lMat;
    }
}
