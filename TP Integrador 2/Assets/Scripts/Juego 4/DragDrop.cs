using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using TMPro;

public class DragDrop : MonoBehaviour
{
    public GameObject objectToDrag;
    public GameObject objectDragToPos;
    public GameObject gameOverPanel;
    public GameObject secondGameOverPanel;
    public GameObject secondCanvasGameOverPanel; // Panel de Game Over del segundo Canvas
    public GameObject secondCanvasWinPanel; // Panel de Victoria del segundo Canvas
    public GameObject newCanvas;
    public GameObject winPanel;
    public TextMeshProUGUI timerText;
    public TextMeshProUGUI secondTimerText; // Referencia al TextMeshProUGUI del segundo Canvas
    public float dropDistance;
    public Vector2 objectInitPos;

    private bool isGameOver = false;
    private bool isWin = false;
    private float timer = 40f;
    public float delayBeforeSceneChange = 4f;
    public string nextSceneName;

    private List<GameObject> draggableObjects = new List<GameObject>();

    private void Start()
    {
        Cursor.visible = true;
        Cursor.lockState = CursorLockMode.None;
        objectInitPos = objectToDrag.transform.position;
        draggableObjects.AddRange(GameObject.FindGameObjectsWithTag("DraggableObject"));
    }

    public void DragObject()
    {
        if (!isGameOver && !isWin)
        {
            objectToDrag.transform.position = Input.mousePosition;
        }
    }

    public void DropObject()
    {
        if (!isGameOver && !isWin)
        {
            float distance = Vector3.Distance(objectToDrag.transform.position, objectDragToPos.transform.position);
            if (distance < dropDistance)
            {
                isGameOver = true;
                objectToDrag.transform.position = objectDragToPos.transform.position;
                objectToDrag.transform.localScale = objectDragToPos.transform.localScale;
                CheckWinCondition();
            }
            else
            {
                objectToDrag.transform.position = objectInitPos;
            }
        }
    }

    private void CheckWinCondition()
    {
        foreach (GameObject draggableObject in draggableObjects)
        {
            if (!draggableObject.GetComponent<DragDrop>().isGameOver)
            {
                return;
            }
        }

        isWin = true;
        if (newCanvas.activeSelf) // Si el segundo canvas est� activo
        {
            ShowSecondCanvasWinPanel(); // Mostrar el panel de Victoria del segundo Canvas
        }
        else
        {
            ShowWinPanel(); // Si no, mostrar el panel de Victoria del primer Canvas
        }
        StartCoroutine(ChangeSceneAfterDelay());
    }

    private void ShowSecondCanvasWinPanel()
    {
        secondCanvasWinPanel.SetActive(true);
    }

    private void Update()
    {
        if (!isGameOver && !isWin)
        {
            timer -= Time.deltaTime;
            if (timer <= 0)
            {
                isGameOver = true;
                if (newCanvas.activeSelf) // Si el segundo canvas est� activo
                {
                    ShowSecondCanvasGameOverPanel(); // Mostrar el panel de Game Over del segundo Canvas
                    Invoke("RestartScene", 3f); // Reiniciar el juego despu�s de 3 segundos
                }
                else
                {
                    ShowGameOverPanel(); // Si no, mostrar el panel de Game Over del primer Canvas
                }
            }
            UpdateTimerDisplay();
        }

        if (isGameOver && !newCanvas.activeSelf) // Si Game Over y el segundo Canvas no est� activo
        {
            if (Input.GetKeyDown(KeyCode.R))
            {
                RestartScene();
            }
        }
    }

    private void UpdateTimerDisplay()
    {
        int seconds = Mathf.CeilToInt(timer);
        timerText.text = seconds.ToString();
        secondTimerText.text = seconds.ToString(); // Actualiza el timer en el segundo Canvas
    }

    private void ShowGameOverPanel()
    {
        gameOverPanel.SetActive(true);
        Invoke("ShowSecondGameOverPanel", 3f);
    }

    private void ShowSecondGameOverPanel()
    {
        secondGameOverPanel.SetActive(true);
        Invoke("ShowNewCanvas", 3f);
    }

    private void ShowNewCanvas()
    {
        newCanvas.SetActive(true);
    }

    private void ShowSecondCanvasGameOverPanel()
    {
        secondCanvasGameOverPanel.SetActive(true);
        Invoke("SwitchToFirstCanvas", 3f); // Llama al m�todo SwitchToFirstCanvas despu�s de 3 segundos
    }

    private void SwitchToFirstCanvas()
    {
        // Desactiva el segundo Canvas y todos sus paneles
        newCanvas.SetActive(false);
        secondCanvasGameOverPanel.SetActive(false);
        secondCanvasWinPanel.SetActive(false);

        // Restablece el temporizador
        timer = 40f;
        UpdateTimerDisplay(); // Actualiza la visualizaci�n del temporizador en ambos Canvases

        // Activa el primer Canvas y el panel de Game Over del primer Canvas
        gameOverPanel.SetActive(true);
    }

    private void ShowWinPanel()
    {
        winPanel.SetActive(true);
    }

    private void RestartScene()
    {
        if (newCanvas.activeSelf) // Si el segundo canvas est� activo
        {
            // Desactiva los paneles activos en el segundo Canvas
            secondCanvasGameOverPanel.SetActive(false);
            secondCanvasWinPanel.SetActive(false);

            // Restablece el temporizador
            timer = 40f;
            UpdateTimerDisplay(); // Actualiza la visualizaci�n del temporizador en ambos Canvases

            isGameOver = false;
            isWin = false;

            // Restablece las posiciones de los objetos arrastrables
            foreach (GameObject draggableObject in draggableObjects)
            {
                draggableObject.GetComponent<DragDrop>().ResetPosition();
            }
        }
        else
        {
            // Si el segundo Canvas no est� activo, recarga la escena como lo est�s haciendo ahora
            SceneManager.LoadScene(SceneManager.GetActiveScene().name);
        }
    }

    // Este m�todo se deber�a a�adir en tu clase para restablecer la posici�n de los objetos arrastrables
    public void ResetPosition()
    {
        objectToDrag.transform.position = objectInitPos;
        isGameOver = false;
    }

    private IEnumerator ChangeSceneAfterDelay()
    {
        yield return new WaitForSeconds(delayBeforeSceneChange);
        SceneManager.LoadScene(nextSceneName);
    }
}
