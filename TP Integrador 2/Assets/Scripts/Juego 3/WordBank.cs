using System.Collections;
using System.Linq;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using UnityEngine.SceneManagement;

public class WordBank : MonoBehaviour
{
    public TextMeshProUGUI timerText;
    public TextMeshProUGUI penaltyText;
    public GameObject gameOverPanel;
    public GameObject additionalPanel;
    public float delayBeforeSceneChange = 2f;
    public string nextSceneName;
    public GameObject winPanel;
    public int numberOfWordsInGame = 50;

    public TMP_Text wordCounterText;

    public Button retryButton;


    private List<string> allWords = new List<string>()
    {
        "Casa", "Perro", "Estudio", "Videojuegos", "Motores", "Comida", "Unity", "Float", "Sistema", "Tecnologia", "Programacion", "Shaders", "Polybrush", "Telefono", "Lista", "Public", "Letras","Eventos", "Tooling", "Discord", "Ram", "Joystick", "Vaso", "Bandera", "Cable", "Mecanicas",
        "Manzana", "Pantalon", "Sandia", "Timer", "Luz", "Flecha", "Carpeta", "Coma", "Comillas", "Puntos", "Espacio", "F1", "9273", "85412", "301111", "Portal", "Dos", "Jugo", "Campana", "Regla", "Awake", "Panel", "46281", "Verde", "Incorrecto", "Correcto", "Barra", "Espada", "Impresora",
    };

    private List<string> originalWords = new List<string>();
    private List<string> workingWords = new List<string>();
    private bool isTimerActive = false;
    private float timer = 60f;
    private bool isGameOver = false;
    private string currentWord;
    private int correctWordsCount = 0; // Variable para contar las palabras correctas

    private void Awake()
    {
        Shuffle(allWords);
        originalWords = allWords.Take(numberOfWordsInGame).ToList();
        workingWords.AddRange(originalWords);
        Shuffle(workingWords);
        ConvertToLower(workingWords);
    }

    private void Start()
    {
        StartTimer();
        DisplayNextWord();
    }

    private void Shuffle(List<string> list)
    {
        for (int i = 0; i < list.Count; i++)
        {
            int random = Random.Range(i, list.Count);
            string temporary = list[i];

            list[i] = list[random];
            list[random] = temporary;
        }
    }

    private void ConvertToLower(List<string> list)
    {
        for (int i = 0; i < list.Count; i++)
        {
            list[i] = list[i].ToLower();
        }
    }

    public void StartTimer()
    {
        isTimerActive = true;
    }

    public string GetWord()
    {
        string newWord = string.Empty;

        if (workingWords.Count != 0)
        {
            newWord = workingWords.Last();
            workingWords.Remove(newWord);

            if (workingWords.Count == 0)
            {
                isTimerActive = false;
                ShowWinPanel();
                StartCoroutine(CompleteGame());
            }
        }

        return newWord;
    }

    private void Update()
    {
        if (isTimerActive && !isGameOver)
        {
            timer -= Time.deltaTime;
            if (timer <= 0)
            {
                isTimerActive = false;
                isGameOver = true;
                ShowGameOverPanel();
            }
            UpdateTimerDisplay();
        }

        if (isGameOver)
        {
            if (Input.GetKeyDown(KeyCode.R))
            {
                RestartScene();
            }
        }

        if (!isGameOver)
        {
            CheckPlayerInput();
        }
    }

    private void UpdateTimerDisplay()
    {
        int seconds = Mathf.CeilToInt(timer);
        timerText.text = seconds.ToString();
    }

    private void DisplayNextWord()
    {
        currentWord = GetWord();
    }

    private void CheckPlayerInput()
    {
        if (Input.GetKeyDown(KeyCode.Return))
        {
            string playerInput = Input.inputString;
            playerInput = playerInput.ToLower();

            if (playerInput == currentWord.ToLower())
            {
                correctWordsCount++; // Incrementa el contador de palabras correctas
                wordCounterText.text = correctWordsCount + "0/23"; // Actualiza el contador en la interfaz

                // Si el jugador ha completado 20 palabras correctamente, muestra el panel de ganaste
                if (correctWordsCount >= 20)
                {
                    ShowWinPanel();
                    isGameOver = true; // Aseg�rate de que el juego se detenga cuando el jugador gane
                    return; // No queremos mostrar la siguiente palabra si el jugador ya gan�
                }

                DisplayNextWord();
            }
            else
            {
                ApplyPenalty();
            }
        }
    }


    public void ApplyPenalty()
    {
        timer -= 2f;
        StartCoroutine(ShowPenalty());
        UpdateTimerDisplay();
    }

    private IEnumerator ShowPenalty()
    {
        penaltyText.text = "-2";
        penaltyText.gameObject.SetActive(true);
        yield return new WaitForSeconds(2f);
        penaltyText.gameObject.SetActive(false);
    }

    private void ShowGameOverPanel()
    {
        gameOverPanel.SetActive(true);
        additionalPanel.SetActive(true);
        retryButton.gameObject.SetActive(true); // Activa el bot�n de reintentar
    }


    private void ShowWinPanel()
    {
        winPanel.SetActive(true);
    }

    private void RestartScene()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }

    private IEnumerator CompleteGame()
    {
        yield return new WaitForSeconds(delayBeforeSceneChange);
        SceneManager.LoadScene(nextSceneName);
    }
}

